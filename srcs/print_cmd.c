/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_cmd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 17:11:42 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/03 19:07:55 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void	print_cmd(const t_cmd *cmd, int size)
{
	int		i;

	i = 0;
	ft_printf("\nStandard commands:\n");
	while (i < size && cmd[i].type == STD)
	{
		ft_printf("%s\n", cmd[i].name);
		i++;
	}
	ft_printf("\nMessage Digest commands:\n");
	while (i < size && cmd[i].type == MD)
	{
		ft_printf("%s\n", cmd[i].name);
		i++;
	}
	ft_printf("\nCipher commands:\n");
	while (i < size && cmd[i].type == CIPHER)
	{
		ft_printf("%s\n", cmd[i].name);
		i++;
	}
}
