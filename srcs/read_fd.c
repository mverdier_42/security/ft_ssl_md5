/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_fd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/07 14:45:40 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/14 15:59:29 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <errno.h>
#include "libft.h"

int		read_fd(int fd, char **buff, uint32_t *bufflen, uint32_t *size)
{
	if ((*bufflen = read(fd, *buff, *bufflen)) > 0)
	{
		*size += *bufflen;
		return (1);
	}
	else if (*bufflen == 0)
		return (0);
	else if (*bufflen < 0)
	{
		ft_printf("ft_ssl: %s\n", strerror(errno));
		exit(1);
	}
	return (0);
}
