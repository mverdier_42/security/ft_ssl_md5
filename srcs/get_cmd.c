/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_cmd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 17:11:51 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 18:37:19 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

const t_cmd	*get_cmd(int *size)
{
	static const t_cmd	cmd[] = {
		{"md5", MD, &parse_md5},
		{"sha224", MD, &parse_sha224},
		{"sha256", MD, &parse_sha256},
		{"sha384", MD, &parse_sha384},
		{"sha512", MD, &parse_sha512}
	};

	*size = sizeof(cmd) / sizeof(t_cmd);
	return (cmd);
}
