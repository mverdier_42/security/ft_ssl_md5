/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha224_get_flags.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 18:37:53 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 18:38:16 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha224.h"

const t_md_flags	*sha224_get_flags(int *size)
{
	static const t_md_flags	flags[] = {
		{'p', FLAG_P, &sha224_stdin}, {'q', FLAG_Q, NULL},
		{'r', FLAG_R, NULL}, {'s', FLAG_S, &sha224_string}
	};

	*size = sizeof(flags) / sizeof(t_md_flags);
	return (flags);
}
