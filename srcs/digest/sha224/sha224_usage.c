/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha224_usage.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 18:33:39 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 18:35:54 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha224.h"

void	sha224_usage(char *token)
{
	if (token != NULL)
		ft_printf("ft_ssl: sha224: illegal option -- %c\n", *token);
	ft_printf("usage: ft_ssl sha224 [-pqr] [-s string] [files ...]\n");
}
