/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha224_string.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 18:43:07 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 18:48:26 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha224.h"

static void	print_hash(char *str, int flags, t_sha256 infos)
{
	if (!(flags & FLAG_Q) && !(flags & FLAG_R))
		ft_printf("SHA224 (\"%s\") = ", str);
	ft_printf("%.8x%.8x%.8x%.8x%.8x%.8x%.8x", infos.h[0], infos.h[1],
		infos.h[2], infos.h[3], infos.h[4], infos.h[5], infos.h[6]);
	if (!(flags & FLAG_Q) && (flags & FLAG_R))
		ft_printf(" \"%s\"", str);
	ft_printf("\n");
}

static void	process_block(t_sha256 *infos, char *str, size_t i)
{
	char	buff[64];
	size_t	bufflen;

	infos->msg = str + i;
	bufflen = ft_strlen(infos->msg);
	if (bufflen >= 64)
		sha256(infos, 64);
	else if (bufflen < 64)
	{
		ft_memcpy(buff, infos->msg, bufflen);
		infos->msg = buff;
		sha256(infos, bufflen);
	}
	if (i + 64 >= infos->size && !infos->end)
	{
		infos->msg = buff;
		sha256(infos, 0);
	}
}

void		sha224_string(char ***av, char **token, int flags)
{
	t_sha256	infos;
	char		*str;
	size_t		i;

	str = *(*token + 1) != '\0' ? ++*token : *(++*av);
	infos.msg = str;
	sha224_init(&infos);
	i = 0;
	infos.size = ft_strlen(infos.msg);
	while (i < infos.size)
	{
		process_block(&infos, str, i);
		i += 64;
	}
	print_hash(str, flags, infos);
	*token += ft_strlen(*token) - 1;
}
