/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_md_flags.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 15:01:17 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/10 14:03:33 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "md_infos.h"

static int	check_flag(const t_md_flags *(*get_flags)(int *), char ***av,
			char **token, int *flags)
{
	int					i;
	int					size;
	const t_md_flags	*md_flags;

	i = -1;
	md_flags = get_flags(&size);
	while (++i < size)
	{
		if (**token == md_flags[i].flag)
		{
			if (!(*flags & md_flags[i].value))
				*flags += md_flags[i].value;
			if (md_flags[i].func)
				(md_flags[i].func)(av, token, *flags);
			return (1);
		}
	}
	return (0);
}

static void	check_flags(const t_md_flags *(*get_flags)(int *),
			void (*usage)(char*), char ***av, int *flags)
{
	char	*token;
	char	*tmp;

	token = **av;
	tmp = token;
	while (*(++token))
	{
		if (!ft_strcmp(**av, "--"))
		{
			if (!(*flags & FLAG_FILE))
				*flags += FLAG_FILE;
			return ;
		}
		if (!check_flag(get_flags, av, &token, flags))
		{
			usage(token);
			exit(0);
		}
	}
	if (token - 1 == tmp)
	{
		usage(tmp);
		exit(0);
	}
}

void		parse_md_flags(char **av, const t_md_flags *(*get_flags)(int *),
			void (*usage)(char*), void (*md_file)(char *, int))
{
	int					flags;
	const t_md_flags	*md_flags;

	flags = 0;
	while (*(++av))
	{
		if (**av == '-' && !(flags & FLAG_FILE))
			check_flags(get_flags, usage, &av, &flags);
		else
		{
			if (!(flags & FLAG_FILE))
				flags += FLAG_FILE;
			md_file(*av, flags);
		}
	}
	if (!(flags & FLAG_S) && !(flags & FLAG_P) && !(flags & FLAG_FILE))
	{
		md_flags = get_flags(&flags);
		md_flags[0].func(NULL, NULL, 0);
	}
}
