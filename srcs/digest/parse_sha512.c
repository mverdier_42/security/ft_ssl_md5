/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_sha512.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 14:34:46 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 14:35:49 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha512.h"

void	parse_sha512(char **av)
{
	parse_md_flags(av, &sha512_get_flags, &sha512_usage, &sha512_file);
}
