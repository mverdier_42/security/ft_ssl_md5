/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha384_get_flags.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 17:41:01 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 17:41:38 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha384.h"

const t_md_flags	*sha384_get_flags(int *size)
{
	static const t_md_flags	flags[] = {
		{'p', FLAG_P, &sha384_stdin}, {'q', FLAG_Q, NULL},
		{'r', FLAG_R, NULL}, {'s', FLAG_S, &sha384_string}
	};

	*size = sizeof(flags) / sizeof(t_md_flags);
	return (flags);
}
