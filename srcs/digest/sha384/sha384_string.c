/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha384_string.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 17:42:12 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 17:53:10 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha384.h"

static void	print_hash(char *str, int flags, t_sha512 infos)
{
	if (!(flags & FLAG_Q) && !(flags & FLAG_R))
		ft_printf("SHA384 (\"%s\") = ", str);
	ft_printf("%.16lx%.16lx%.16lx%.16lx%.16lx%.16lx",
		infos.h[0], infos.h[1], infos.h[2], infos.h[3], infos.h[4], infos.h[5]);
	if (!(flags & FLAG_Q) && (flags & FLAG_R))
		ft_printf(" \"%s\"", str);
	ft_printf("\n");
}

static void	process_block(t_sha512 *infos, char *str, size_t i)
{
	char	buff[128];
	size_t	bufflen;

	infos->msg = str + i;
	bufflen = ft_strlen(infos->msg);
	if (bufflen >= 128)
		sha512(infos, 128);
	else if (bufflen < 128)
	{
		ft_memcpy(buff, infos->msg, bufflen);
		infos->msg = buff;
		sha512(infos, bufflen);
	}
	if (i + 128 >= infos->size && !infos->end)
	{
		infos->msg = buff;
		sha512(infos, 0);
	}
}

void		sha384_string(char ***av, char **token, int flags)
{
	t_sha512	infos;
	char		*str;
	size_t		i;

	str = *(*token + 1) != '\0' ? ++*token : *(++*av);
	infos.msg = str;
	sha384_init(&infos);
	i = 0;
	infos.size = ft_strlen(infos.msg);
	while (i < infos.size)
	{
		process_block(&infos, str, i);
		i += 128;
	}
	print_hash(str, flags, infos);
	*token += ft_strlen(*token) - 1;
}
