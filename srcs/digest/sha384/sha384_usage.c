/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha384_usage.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 17:39:39 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 17:40:29 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha384.h"

void	sha384_usage(char *token)
{
	if (token != NULL)
		ft_printf("ft_ssl: sha384: illegal option -- %c\n", *token);
	ft_printf("usage: ft_ssl sha384 [-pqr] [-s string] [files ...]\n");
}
