/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha384_file.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 17:44:17 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 17:53:51 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha384.h"
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

static void	print_hash(char *name, int flags, t_sha512 infos)
{
	if (!(flags & FLAG_Q) && !(flags & FLAG_R))
		ft_printf("SHA384 (%s) = ", name);
	ft_printf("%.16lx%.16lx%.16lx%.16lx%.16lx%.16lx",
		infos.h[0], infos.h[1], infos.h[2], infos.h[3], infos.h[4], infos.h[5]);
	if (!(flags & FLAG_Q) && (flags & FLAG_R))
		ft_printf(" %s", name);
	ft_printf("\n");
}

static void	cut_blocks(t_sha512 *infos, uint32_t bufflen)
{
	char		*buffer;
	uint32_t	i;

	buffer = infos->msg;
	i = 0;
	while (i < bufflen)
	{
		infos->msg = buffer + i;
		sha512(infos, (bufflen - i < 128 ? bufflen - i : 128));
		i += 128;
	}
}

void		sha384_file(char *name, int flags)
{
	t_sha512	infos;
	char		buffer[2048];
	uint32_t	bufflen;
	int			fd;

	if ((fd = open(name, O_RDONLY)) < 0)
	{
		ft_printf("ft_ssl: sha384: %s: %s\n", name, strerror(errno));
		return ;
	}
	infos.msg = buffer;
	infos.size = 0;
	sha384_init(&infos);
	bufflen = 2048;
	while (read_fd(fd, &infos.msg, &bufflen, &infos.size))
	{
		cut_blocks(&infos, bufflen);
		bufflen = 2048;
		infos.msg = buffer;
	}
	if (!infos.end)
		sha512(&infos, 0);
	close(fd);
	print_hash(name, flags, infos);
}
