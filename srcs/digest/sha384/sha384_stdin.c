/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha384_stdin.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 17:43:23 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 17:53:33 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha384.h"

void	sha384_stdin(char ***av, char **token, int flags)
{
	char		buffer[128];
	uint32_t	bufflen;
	t_sha512	infos;

	(void)av;
	(void)token;
	bufflen = 128;
	infos.msg = buffer;
	infos.size = 0;
	sha384_init(&infos);
	while (read_fd(0, &infos.msg, &bufflen, &infos.size))
	{
		if ((flags & FLAG_P))
			write(1, infos.msg, bufflen);
		sha512(&infos, bufflen);
		bufflen = 128;
	}
	if (!infos.end)
		sha512(&infos, 0);
	ft_printf("%.16lx%.16lx%.16lx%.16lx%.16lx%.16lx\n",
		infos.h[0], infos.h[1], infos.h[2], infos.h[3], infos.h[4], infos.h[5]);
}
