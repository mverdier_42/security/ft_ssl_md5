/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256_hash_functions.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 14:08:32 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 14:25:13 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha256.h"

uint32_t	sha256_ch(uint32_t e, uint32_t f, uint32_t g)
{
	return ((e & f) ^ (~e & g));
}

uint32_t	sha256_maj(uint32_t a, uint32_t b, uint32_t c)
{
	return ((a & b) ^ (a & c) ^ (b & c));
}

uint32_t	sha256_sigma0(uint32_t n)
{
	return (rot_right(n, 2) ^ rot_right(n, 13) ^ rot_right(n, 22));
}

uint32_t	sha256_sigma1(uint32_t n)
{
	return (rot_right(n, 6) ^ rot_right(n, 11) ^ rot_right(n, 25));
}
