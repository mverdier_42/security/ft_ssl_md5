/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256_get_flags.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 11:10:38 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/14 14:17:23 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha256.h"

const t_md_flags	*sha256_get_flags(int *size)
{
	static const t_md_flags	flags[] = {
		{'p', FLAG_P, &sha256_stdin}, {'q', FLAG_Q, NULL},
		{'r', FLAG_R, NULL}, {'s', FLAG_S, &sha256_string}
	};

	*size = sizeof(flags) / sizeof(t_md_flags);
	return (flags);
}
