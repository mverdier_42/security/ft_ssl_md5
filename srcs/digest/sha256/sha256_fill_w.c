/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256_fill_w.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 13:54:56 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 13:55:47 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha256.h"

static uint32_t			sigma_min0(uint32_t n)
{
	return (rot_right(n, 7) ^ rot_right(n, 18) ^ (n >> 3));
}

static uint32_t			sigma_min1(uint32_t n)
{
	return (rot_right(n, 17) ^ rot_right(n, 19) ^ (n >> 10));
}

void					sha256_fill_w(t_sha256 *infos)
{
	uint32_t	i;

	i = 0;
	while (i < 64)
	{
		if (i < 16)
			infos->w[i] = swap_uint32(*(uint32_t*)(infos->msg + i * 4));
		else
			infos->w[i] = sigma_min1(infos->w[i - 2]) + infos->w[i - 7]
				+ sigma_min0(infos->w[i - 15]) + infos->w[i - 16];
		i++;
	}
	print_sha256_w(infos->w);
}
