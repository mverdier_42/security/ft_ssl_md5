/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256_file.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 11:22:40 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 12:01:45 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha256.h"
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

static void	print_hash(char *name, int flags, t_sha256 infos)
{
	if (!(flags & FLAG_Q) && !(flags & FLAG_R))
		ft_printf("SHA256 (%s) = ", name);
	ft_printf("%.8x%.8x%.8x%.8x%.8x%.8x%.8x%.8x", infos.h[0], infos.h[1],
		infos.h[2], infos.h[3], infos.h[4], infos.h[5], infos.h[6], infos.h[7]);
	if (!(flags & FLAG_Q) && (flags & FLAG_R))
		ft_printf(" %s", name);
	ft_printf("\n");
}

static void	cut_blocks(t_sha256 *infos, uint32_t bufflen)
{
	char		*buffer;
	uint32_t	i;

	buffer = infos->msg;
	i = 0;
	while (i < bufflen)
	{
		infos->msg = buffer + i;
		sha256(infos, (bufflen - i < 64 ? bufflen - i : 64));
		i += 64;
	}
}

void		sha256_file(char *name, int flags)
{
	t_sha256	infos;
	char		buffer[2048];
	uint32_t	bufflen;
	int			fd;

	if ((fd = open(name, O_RDONLY)) < 0)
	{
		ft_printf("ft_ssl: sha256: %s: %s\n", name, strerror(errno));
		return ;
	}
	infos.msg = buffer;
	infos.size = 0;
	sha256_init(&infos);
	bufflen = 2048;
	while (read_fd(fd, &infos.msg, &bufflen, &infos.size))
	{
		cut_blocks(&infos, bufflen);
		bufflen = 2048;
		infos.msg = buffer;
	}
	if (!infos.end)
		sha256(&infos, 0);
	close(fd);
	print_hash(name, flags, infos);
}
