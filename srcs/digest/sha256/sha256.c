/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 11:24:54 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 13:40:59 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha256.h"
#include <errno.h>

void	sha256(t_sha256 *infos, uint32_t len)
{
	if (len < 64)
	{
		ft_bzero(infos->msg + len, 64 - len);
		if (!infos->bit_pad)
		{
			*(infos->msg + len) = -128;
			infos->bit_pad = 1;
		}
		if (len + 9 <= 64)
		{
			infos->end = 1;
			*(uint64_t*)(infos->msg + 56) = swap_uint64(infos->size * 8);
		}
	}
	print_sha256_blocks(infos->msg);
	sha256_hash(infos);
}
