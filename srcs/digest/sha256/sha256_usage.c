/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256_usage.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 11:16:17 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/11 11:17:14 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha256.h"

void	sha256_usage(char *token)
{
	if (token != NULL)
		ft_printf("ft_ssl: sha256: illegal option -- %c\n", *token);
	ft_printf("usage: ft_ssl sha256 [-pqr] [-s string] [files ...]\n");
}
