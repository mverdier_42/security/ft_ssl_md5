/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256_stdin.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 11:17:55 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 11:38:07 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha256.h"

void	sha256_stdin(char ***av, char **token, int flags)
{
	char		buffer[64];
	uint32_t	bufflen;
	t_sha256	infos;

	(void)av;
	(void)token;
	bufflen = 64;
	infos.msg = buffer;
	infos.size = 0;
	sha256_init(&infos);
	while (read_fd(0, &infos.msg, &bufflen, &infos.size))
	{
		if ((flags & FLAG_P))
			write(1, infos.msg, bufflen);
		sha256(&infos, bufflen);
		bufflen = 64;
	}
	if (!infos.end)
		sha256(&infos, 0);
	ft_printf("%.8x%.8x%.8x%.8x%.8x%.8x%.8x%.8x\n", infos.h[0], infos.h[1],
		infos.h[2], infos.h[3], infos.h[4], infos.h[5], infos.h[6], infos.h[7]);
}
