/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256_debug.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 12:25:04 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 13:52:04 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha256.h"

void	print_sha256_w(uint32_t *w)
{
	uint32_t	i;

	i = 0;
	if (!DEBUG_SHA256)
		return ;
	while (i < 16)
	{
		ft_printf("w[%2d] = %.8x\n", i, w[i]);
		i++;
	}
	ft_printf("\n");
}

void	print_sha256_val(t_sha256_val val, uint32_t i, uint32_t init)
{
	if (!DEBUG_SHA256)
		return ;
	if (init)
		ft_printf("init:");
	else
		ft_printf("i = %2d", i);
	ft_printf("\t%.8x  %.8x  %.8x  %.8x  %.8x  %.8x  %.8x  %.8x\n", val.a,
		val.b, val.c, val.d, val.e, val.f, val.g, val.h);
	if (i == 31 || i == 63)
		ft_printf("\n");
}

void	print_sha256_block_processed(t_sha256_val val, t_sha256 infos)
{
	if (!DEBUG_SHA256)
		return ;
	ft_printf("H1 = %.8x + %.8x = %.8x\n", infos.h[0], val.a, infos.h[0]
		+ val.a);
	ft_printf("H2 = %.8x + %.8x = %.8x\n", infos.h[1], val.a, infos.h[1]
		+ val.b);
	ft_printf("H3 = %.8x + %.8x = %.8x\n", infos.h[2], val.c, infos.h[2]
		+ val.c);
	ft_printf("H4 = %.8x + %.8x = %.8x\n", infos.h[3], val.d, infos.h[3]
		+ val.d);
	ft_printf("H5 = %.8x + %.8x = %.8x\n", infos.h[4], val.e, infos.h[4]
		+ val.e);
	ft_printf("H6 = %.8x + %.8x = %.8x\n", infos.h[5], val.f, infos.h[5]
		+ val.f);
	ft_printf("H7 = %.8x + %.8x = %.8x\n", infos.h[6], val.g, infos.h[6]
		+ val.g);
	ft_printf("H8 = %.8x + %.8x = %.8x\n\n", infos.h[7], val.h, infos.h[7]
		+ val.h);
}

void	print_sha256_blocks(char *str)
{
	static int	n = 0;
	int			i;

	i = 0;
	if (!DEBUG_SHA256)
		return ;
	ft_printf("Block %d:\n", n);
	while (i < 64)
	{
		ft_printf("[%2d]\t%.8x\n", i / 4, swap_uint32(*(uint32_t*)(str + i)));
		i += 4;
	}
	ft_printf("\n");
	n++;
}
