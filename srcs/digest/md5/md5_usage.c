/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5_usage.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 14:46:41 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/04 14:00:14 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "md5.h"

void	md5_usage(char *token)
{
	if (token != NULL)
		ft_printf("ft_ssl: md5: illegal option -- %c\n", *token);
	ft_printf("usage: ft_ssl md5 [-pqr] [-s string] [files ...]\n");
}
