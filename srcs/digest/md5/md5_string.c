/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5_string.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 14:05:32 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/14 19:04:06 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "md5.h"

static void	print_hash(char *str, int flags, t_md5 infos)
{
	infos.h[0] = swap_uint32(infos.h[0]);
	infos.h[1] = swap_uint32(infos.h[1]);
	infos.h[2] = swap_uint32(infos.h[2]);
	infos.h[3] = swap_uint32(infos.h[3]);
	if (!(flags & FLAG_Q) && !(flags & FLAG_R))
		ft_printf("MD5 (\"%s\") = ", str);
	ft_printf("%.8x%.8x%.8x%.8x", infos.h[0], infos.h[1], infos.h[2],
		infos.h[3]);
	if (!(flags & FLAG_Q) && (flags & FLAG_R))
		ft_printf(" \"%s\"", str);
	ft_printf("\n");
}

static void	process_block(t_md5 *infos, char *str, size_t i)
{
	char	buff[64];
	size_t	bufflen;

	infos->msg = str + i;
	bufflen = ft_strlen(infos->msg);
	if (bufflen >= 64)
		md5(infos, 64);
	else if (bufflen < 64)
	{
		ft_memcpy(buff, infos->msg, bufflen);
		infos->msg = buff;
		md5(infos, bufflen);
	}
	if (i + 64 >= infos->size && !infos->end)
	{
		infos->msg = buff;
		md5(infos, 0);
	}
}

void		md5_string(char ***av, char **token, int flags)
{
	t_md5	infos;
	char	*str;
	size_t	i;

	str = *(*token + 1) != '\0' ? ++*token : *(++*av);
	infos.msg = str;
	md5_init(&infos);
	i = 0;
	infos.size = ft_strlen(infos.msg);
	while (i < infos.size)
	{
		process_block(&infos, str, i);
		i += 64;
	}
	print_hash(str, flags, infos);
	*token += ft_strlen(*token) - 1;
}
