/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5_hash.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/09 19:33:37 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/14 17:49:00 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "md5.h"

static void	get_fg(t_md5_val *val, int i)
{
	if (i < 16)
	{
		val->f = (val->b & val->c) | ((~val->b) & val->d);
		val->g = i;
	}
	else if (i < 32)
	{
		val->f = (val->d & val->b) | ((~val->d) & val->c);
		val->g = ((5 * i) + 1) % 16;
	}
	else if (i < 48)
	{
		val->f = val->b ^ val->c ^ val->d;
		val->g = ((3 * i) + 5) % 16;
	}
	else if (i < 64)
	{
		val->f = val->c ^ (val->b | (~val->d));
		val->g = (7 * i) % 16;
	}
}

void		md5_hash(t_md5 *hdr)
{
	t_md5_val	val;
	uint32_t	i;

	val.a = hdr->h[0];
	val.b = hdr->h[1];
	val.c = hdr->h[2];
	val.d = hdr->h[3];
	i = 0;
	while (i < 64)
	{
		get_fg(&val, i);
		val.tmp = val.d;
		val.d = val.c;
		val.c = val.b;
		val.b = val.a + val.f + hdr->k[i] + *(uint32_t*)(hdr->msg + val.g * 4);
		val.b = rot_left(val.b, hdr->r[i]) + val.c;
		val.a = val.tmp;
		print_md5_vals(val, i);
		i++;
	}
	hdr->h[0] += val.a;
	hdr->h[1] += val.b;
	hdr->h[2] += val.c;
	hdr->h[3] += val.d;
	print_md5_processed_words(hdr->h);
}
