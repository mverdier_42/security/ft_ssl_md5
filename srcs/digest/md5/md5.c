/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 18:22:01 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/14 17:30:21 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "md5.h"

void					md5(t_md5 *infos, uint32_t len)
{
	if (len < 64)
	{
		ft_bzero(infos->msg + len, 64 - len);
		if (!infos->bit_pad)
		{
			*(infos->msg + len) = -128;
			infos->bit_pad = 1;
		}
		if (len + 9 <= 64)
		{
			infos->end = 1;
			*(uint64_t*)(infos->msg + 56) = (uint64_t)(infos->size * 8);
		}
	}
	print_md5_blocks(infos->msg);
	md5_hash(infos);
}
