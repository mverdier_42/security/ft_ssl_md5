/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5_file.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/07 14:00:50 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 11:17:34 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "md5.h"
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

static void	print_hash(char *name, int flags, t_md5 infos)
{
	infos.h[0] = swap_uint32(infos.h[0]);
	infos.h[1] = swap_uint32(infos.h[1]);
	infos.h[2] = swap_uint32(infos.h[2]);
	infos.h[3] = swap_uint32(infos.h[3]);
	if (!(flags & FLAG_Q) && !(flags & FLAG_R))
		ft_printf("MD5 (%s) = ", name);
	ft_printf("%.8x%.8x%.8x%.8x", infos.h[0], infos.h[1], infos.h[2],
		infos.h[3]);
	if (!(flags & FLAG_Q) && (flags & FLAG_R))
		ft_printf(" %s", name);
	ft_printf("\n");
}

static void	cut_blocks(t_md5 *infos, uint32_t bufflen)
{
	char		*buffer;
	uint32_t	i;

	buffer = infos->msg;
	i = 0;
	while (i < bufflen)
	{
		infos->msg = buffer + i;
		md5(infos, (bufflen - i < 64 ? bufflen - i : 64));
		i += 64;
	}
}

void		md5_file(char *name, int flags)
{
	t_md5		infos;
	char		buffer[2048];
	uint32_t	bufflen;
	int			fd;

	if ((fd = open(name, O_RDONLY)) < 0)
	{
		ft_printf("ft_ssl: md5: %s: %s\n", name, strerror(errno));
		return ;
	}
	infos.msg = buffer;
	infos.size = 0;
	md5_init(&infos);
	bufflen = 2048;
	while (read_fd(fd, &infos.msg, &bufflen, &infos.size))
	{
		cut_blocks(&infos, bufflen);
		bufflen = 2048;
		infos.msg = buffer;
	}
	if (!infos.end)
		md5(&infos, 0);
	close(fd);
	print_hash(name, flags, infos);
}
