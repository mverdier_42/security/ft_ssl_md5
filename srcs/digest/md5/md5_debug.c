/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5_debug.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/09 16:21:43 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/14 18:50:22 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "md5.h"

void	print_md5_blocks(char *str)
{
	static int	n = 0;
	int			i;

	i = 0;
	if (!DEBUG_MD5)
		return ;
	ft_printf("Block %d:\n", n);
	while (i < 64)
	{
		ft_printf("[%2d]%u\n", i / 4, *(uint32_t*)(str + i));
		i += 4;
	}
	ft_printf("\n");
	n++;
}

void	print_md5_vals(t_md5_val val, int i)
{
	if (!DEBUG_MD5)
		return ;
	ft_printf("[i = %2d] A=%u B=%u C=%u D=%u\n", i, val.a, val.b, val.c, val.d);
}

void	print_md5_processed_words(uint32_t *h)
{
	if (!DEBUG_MD5)
		return ;
	ft_printf("\nProcessed: A=%u B=%u C=%u D=%u\n\n", h[0], h[1], h[2], h[3]);
}
