/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5_stdin.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 14:57:56 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/14 17:41:11 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "md5.h"

static void	print_hash(t_md5 infos)
{
	infos.h[0] = swap_uint32(infos.h[0]);
	infos.h[1] = swap_uint32(infos.h[1]);
	infos.h[2] = swap_uint32(infos.h[2]);
	infos.h[3] = swap_uint32(infos.h[3]);
	ft_printf("%.8x%.8x%.8x%.8x\n", infos.h[0], infos.h[1], infos.h[2],
		infos.h[3]);
}

void		md5_stdin(char ***av, char **token, int flags)
{
	char		buffer[64];
	uint32_t	bufflen;
	t_md5		infos;

	(void)av;
	(void)token;
	bufflen = 64;
	infos.msg = buffer;
	infos.size = 0;
	md5_init(&infos);
	while (read_fd(0, &infos.msg, &bufflen, &infos.size))
	{
		if ((flags & FLAG_P))
			write(1, infos.msg, bufflen);
		md5(&infos, bufflen);
		bufflen = 64;
	}
	if (!infos.end)
		md5(&infos, 0);
	print_hash(infos);
}
