/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5_get_flags.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 14:25:13 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/14 14:16:52 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "md5.h"

const t_md_flags	*md5_get_flags(int *size)
{
	static const t_md_flags	flags[] = {
		{'p', FLAG_P, &md5_stdin}, {'q', FLAG_Q, NULL},
		{'r', FLAG_R, NULL}, {'s', FLAG_S, &md5_string}
	};

	*size = sizeof(flags) / sizeof(t_md_flags);
	return (flags);
}
