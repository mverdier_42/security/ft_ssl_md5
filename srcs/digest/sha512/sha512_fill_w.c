/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512_fill_w.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 14:43:44 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 17:10:10 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha512.h"

static uint64_t			sigma_min0(uint64_t n)
{
	return (rot_right64(n, 1) ^ rot_right64(n, 8) ^ (n >> 7));
}

static uint64_t			sigma_min1(uint64_t n)
{
	return (rot_right64(n, 19) ^ rot_right64(n, 61) ^ (n >> 6));
}

void					sha512_fill_w(t_sha512 *infos)
{
	uint32_t	i;

	i = 0;
	while (i < 80)
	{
		if (i < 16)
			infos->w[i] = swap_uint64(*(uint64_t*)(infos->msg + i * 8));
		else
			infos->w[i] = sigma_min1(infos->w[i - 2]) + infos->w[i - 7]
				+ sigma_min0(infos->w[i - 15]) + infos->w[i - 16];
		i++;
	}
	print_sha512_w(infos->w);
}
