/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 14:40:13 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 17:05:45 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha512.h"
#include <errno.h>

void	sha512(t_sha512 *infos, uint32_t len)
{
	__uint128_t	tmp;

	tmp = 0;
	if (len < 128)
	{
		ft_bzero(infos->msg + len, 128 - len);
		if (!infos->bit_pad)
		{
			*(infos->msg + len) = -128;
			infos->bit_pad = 1;
		}
		if (len + 17 <= 128)
		{
			infos->end = 1;
			tmp = infos->size * 8;
			swap_uint128(&tmp);
			*(__uint128_t*)(infos->msg + 112) = tmp;
		}
	}
	print_sha512_blocks(infos->msg);
	sha512_hash(infos);
}
