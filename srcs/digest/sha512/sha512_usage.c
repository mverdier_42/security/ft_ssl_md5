/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512_usage.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 14:36:55 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 14:38:21 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha512.h"

void	sha512_usage(char *token)
{
	if (token != NULL)
		ft_printf("ft_ssl: sha512: illegal option -- %c\n", *token);
	ft_printf("usage: ft_ssl sha512 [-pqr] [-s string] [files ...]\n");
}
