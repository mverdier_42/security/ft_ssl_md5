/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512_hash_functions.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 14:48:05 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 17:09:54 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha512.h"

uint64_t	sha512_ch(uint64_t e, uint64_t f, uint64_t g)
{
	return ((e & f) ^ (~e & g));
}

uint64_t	sha512_maj(uint64_t a, uint64_t b, uint64_t c)
{
	return ((a & b) ^ (a & c) ^ (b & c));
}

uint64_t	sha512_sigma0(uint64_t n)
{
	return (rot_right64(n, 28) ^ rot_right64(n, 34) ^ rot_right64(n, 39));
}

uint64_t	sha512_sigma1(uint64_t n)
{
	return (rot_right64(n, 14) ^ rot_right64(n, 18) ^ rot_right64(n, 41));
}
