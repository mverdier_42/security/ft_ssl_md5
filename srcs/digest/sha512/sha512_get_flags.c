/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512_get_flags.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 14:38:50 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 14:39:30 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha512.h"

const t_md_flags	*sha512_get_flags(int *size)
{
	static const t_md_flags	flags[] = {
		{'p', FLAG_P, &sha512_stdin}, {'q', FLAG_Q, NULL},
		{'r', FLAG_R, NULL}, {'s', FLAG_S, &sha512_string}
	};

	*size = sizeof(flags) / sizeof(t_md_flags);
	return (flags);
}
