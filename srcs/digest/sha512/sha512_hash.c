/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512_hash.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 14:41:17 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 17:00:37 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha512.h"

static void	init_sha512_val(t_sha512_val *val, t_sha512 *infos)
{
	val->a = infos->h[0];
	val->b = infos->h[1];
	val->c = infos->h[2];
	val->d = infos->h[3];
	val->e = infos->h[4];
	val->f = infos->h[5];
	val->g = infos->h[6];
	val->h = infos->h[7];
}

static void	update_hash(t_sha512 *infos, t_sha512_val val)
{
	infos->h[0] += val.a;
	infos->h[1] += val.b;
	infos->h[2] += val.c;
	infos->h[3] += val.d;
	infos->h[4] += val.e;
	infos->h[5] += val.f;
	infos->h[6] += val.g;
	infos->h[7] += val.h;
}

void		sha512_hash(t_sha512 *infos)
{
	t_sha512_val	val;
	uint32_t		i;

	init_sha512_val(&val, infos);
	sha512_fill_w(infos);
	i = 0;
	print_sha512_val(val, i, 1);
	while (i < 80)
	{
		val.t1 = val.h + sha512_sigma1(val.e) + sha512_ch(val.e, val.f, val.g)
			+ infos->k[i] + infos->w[i];
		val.t2 = sha512_sigma0(val.a) + sha512_maj(val.a, val.b, val.c);
		val.h = val.g;
		val.g = val.f;
		val.f = val.e;
		val.e = val.d + val.t1;
		val.d = val.c;
		val.c = val.b;
		val.b = val.a;
		val.a = val.t1 + val.t2;
		print_sha512_val(val, i, 0);
		i++;
	}
	print_sha512_block_processed(val, *infos);
	update_hash(infos, val);
}
