/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512_debug.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 14:45:23 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 17:01:28 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha512.h"

void	print_sha512_w(uint64_t *w)
{
	uint32_t	i;

	i = 0;
	if (!DEBUG_SHA512)
		return ;
	while (i < 16)
	{
		ft_printf("w[%2d] = %.16lx\n", i, w[i]);
		i++;
	}
	ft_printf("\n");
}

void	print_sha512_val(t_sha512_val val, uint32_t i, uint32_t init)
{
	if (!DEBUG_SHA512)
		return ;
	if (init)
		ft_printf("init:");
	else
		ft_printf("i = %2d", i);
	ft_printf("\t%.16lx %.16lx %.16lx %.16lx %.16lx %.16lx %.16lx %.16lx\n",
		val.a, val.b, val.c, val.d, val.e, val.f, val.g, val.h);
	if (i == 31 || i == 63 || i == 79)
		ft_printf("\n");
}

void	print_sha512_block_processed(t_sha512_val val, t_sha512 infos)
{
	if (!DEBUG_SHA512)
		return ;
	ft_printf("H1 = %.16lx + %.16lx = %.16lx\n", infos.h[0], val.a, infos.h[0]
		+ val.a);
	ft_printf("H2 = %.16lx + %.16lx = %.16lx\n", infos.h[1], val.a, infos.h[1]
		+ val.b);
	ft_printf("H3 = %.16lx + %.16lx = %.16lx\n", infos.h[2], val.c, infos.h[2]
		+ val.c);
	ft_printf("H4 = %.16lx + %.16lx = %.16lx\n", infos.h[3], val.d, infos.h[3]
		+ val.d);
	ft_printf("H5 = %.16lx + %.16lx = %.16lx\n", infos.h[4], val.e, infos.h[4]
		+ val.e);
	ft_printf("H6 = %.16lx + %.16lx = %.16lx\n", infos.h[5], val.f, infos.h[5]
		+ val.f);
	ft_printf("H7 = %.16lx + %.16lx = %.16lx\n", infos.h[6], val.g, infos.h[6]
		+ val.g);
	ft_printf("H8 = %.16lx + %.16lx = %.16lx\n\n", infos.h[7], val.h, infos.h[7]
		+ val.h);
}

void	print_sha512_blocks(char *str)
{
	static int	n = 0;
	int			i;

	i = 0;
	if (!DEBUG_SHA512)
		return ;
	ft_printf("Block %d:\n", n);
	while (i < 128)
	{
		ft_printf("[%2d]\t%.8lx\n", i / 4, swap_uint32(*(uint32_t*)(str + i)));
		i += 4;
	}
	ft_printf("\n");
	n++;
}
