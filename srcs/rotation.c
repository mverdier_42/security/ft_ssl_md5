/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotation.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 15:38:29 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 17:11:09 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "operations.h"

uint32_t	rot_left(uint32_t n, uint32_t i)
{
	return (n << i | n >> (32 - i));
}

uint32_t	rot_right(uint32_t n, uint32_t i)
{
	return (n >> i | n << (32 - i));
}

uint64_t	rot_left64(uint64_t n, uint64_t i)
{
	return (n << i | n >> (64 - i));
}

uint64_t	rot_right64(uint64_t n, uint64_t i)
{
	return (n >> i | n << (64 - i));
}
