/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/09 16:26:32 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 16:47:46 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "operations.h"

uint32_t	swap_uint32(uint32_t val)
{
	return (((val & 0x000000FF) << 24) | ((val & 0x0000FF00) << 8) |
			((val & 0x00FF0000) >> 8) | ((val & 0xFF000000) >> 24));
}

uint64_t	swap_uint64(uint64_t val)
{
	return (
	((val >> 56) & 0x00000000000000FF) | ((val >> 40) & 0x000000000000FF00)
	| ((val >> 24) & 0x0000000000FF0000) | ((val >> 8) & 0x00000000FF000000)
	| ((val << 8) & 0x000000FF00000000) | ((val << 24) & 0x0000FF0000000000)
	| ((val << 40) & 0x00FF000000000000) | ((val << 56) & 0xFF00000000000000));
}

void		swap_uint128(void *val)
{
	char		*p;
	char		tmp;
	uint32_t	lo;
	uint32_t	hi;

	p = val;
	lo = 0;
	hi = 15;
	while (hi > lo)
	{
		tmp = p[lo];
		p[lo] = p[hi];
		p[hi] = tmp;
		lo++;
		hi--;
	}
}
