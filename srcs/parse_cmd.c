/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_cmd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 17:11:38 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/03 19:08:09 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void		parse_cmd(char **av)
{
	const t_cmd	*cmd;
	int			size;
	int			i;

	cmd = get_cmd(&size);
	i = 0;
	while (i < size)
	{
		if (!ft_strcmp(*av, cmd[i].name))
		{
			(cmd[i].func)(av);
			return ;
		}
		i++;
	}
	ft_printf("ft_ssl: Error: '%s' is not a valid command.\n", *av);
	print_cmd(cmd, size);
}
