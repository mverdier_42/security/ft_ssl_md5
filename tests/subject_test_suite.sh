#!/bin/zsh

clear_screen() {
	echo "===================================================="
	echo "Press enter to continue"
	read -n 1
	clear
}

md5_suite() {
	clear
	echo "====================================================\n\n\t\tTests for MD5\n\n===================================================="
	echo '> echo "pickle rick" | ./ft_ssl md5'
	echo "pickle rick" | ../ft_ssl md5
	echo ">"
	echo "pickle rick" | md5
	clear_screen
	echo '> echo "Do not pity the dead, Harry." | ./ft_ssl md5 -p'
	echo "Do not pity the dead, Harry." | ../ft_ssl md5 -p
	echo ">"
	echo "Do not pity the dead, Harry." | md5 -p
	clear_screen
	echo '> echo "Pity the living." | ./ft_ssl md5 -q -r'
	echo "Pity the living." | ../ft_ssl md5 -q -r
	echo ">"
	echo "Pity the living." | md5 -q -r
	clear_screen
	echo '> echo "And above all," > file'
	echo "And above all," > file
	echo '> ./ft_ssl md5 file'
	../ft_ssl md5 file
	echo '> ./ft_ssl md5 -r file'
	../ft_ssl md5 -r file
	echo '>'
	md5 file
	md5 -r file
	clear_screen
	echo "> ./ft_ssl md5 -s \"pity those that aren't following baerista on spotify.\""
	../ft_ssl md5 -s "pity those that aren't following baerista on spotify."
	echo '>'
	md5 -s "pity those that aren't following baerista on spotify."
	clear_screen
	echo '> echo "be sure to handle edge cases carefully" | ../ft_ssl md5 -p file'
	echo "be sure to handle edge cases carefully" | ../ft_ssl md5 -p file
	echo '>'
	echo "be sure to handle edge cases carefully" | md5 -p file
	clear_screen
	echo '> echo "some of this will not make sense at first" | ../ft_ssl md5 file'
	echo "some of this will not make sense at first" | ../ft_ssl md5 file
	echo '>'
	echo "some of this will not make sense at first" | md5 file
	clear_screen
	echo '> echo "but eventually you will understand" | ../ft_ssl md5 -p -r file'
	echo "but eventually you will understand" | ../ft_ssl md5 -p -r file
	echo '>'
	echo "but eventually you will understand" | md5 -p -r file
	clear_screen
	echo "> echo \"GL HF let's go\" | ../ft_ssl md5 -p -s \"foo\" file"
	echo "GL HF let's go" | ../ft_ssl md5 -p -s "foo" file
	echo '>'
	echo "GL HF let's go" | md5 -p -s "foo" file
	clear_screen
	echo '> echo "one more thing" | ../ft_ssl md5 -r -p -s "foo" file -s "bar"'
	echo "one more thing" | ../ft_ssl md5 -r -p -s "foo" file -s "bar"
	echo '>'
	echo "one more thing" | md5 -r -p -s "foo" file -s "bar"
	clear_screen
	echo '> echo "just to be extra clear" | ../ft_ssl md5 -r -q -p -s "foo" file'
	echo "just to be extra clear" | ../ft_ssl md5 -r -q -p -s "foo" file
	echo '>'
	echo "just to be extra clear" | md5 -r -q -p -s "foo" file
	clear_screen
}

sha256_suite() {
	echo "====================================================\n\n\t\tTests for SHA256\n\n===================================================="
	echo '> echo "pickle rick" | ./ft_ssl sha256'
	echo "pickle rick" | ../ft_ssl sha256
	echo ">"
	clear_screen
	echo '> echo "Do not pity the dead, Harry." | ./ft_ssl sha256 -p'
	echo "Do not pity the dead, Harry." | ../ft_ssl sha256 -p
	echo ">"
	clear_screen
	echo '> echo "Pity the living." | ./ft_ssl sha256 -q -r'
	echo "Pity the living." | ../ft_ssl sha256 -q -r
	echo ">"
	clear_screen
	echo '> echo "And above all," > file'
	echo "And above all," > file
	echo '> ./ft_ssl sha256 file'
	../ft_ssl sha256 file
	echo '> ./ft_ssl sha256 -r file'
	../ft_ssl sha256 -r file
	echo '>'
	clear_screen
	echo "> ./ft_ssl sha256 -s \"pity those that aren't following baerista on spotify.\""
	../ft_ssl sha256 -s "pity those that aren't following baerista on spotify."
	echo '>'
	clear_screen
	echo '> echo "be sure to handle edge cases carefully" | ../ft_ssl sha256 -p file'
	echo "be sure to handle edge cases carefully" | ../ft_ssl sha256 -p file
	echo '>'
	clear_screen
	echo '> echo "some of this will not make sense at first" | ../ft_ssl sha256 file'
	echo "some of this will not make sense at first" | ../ft_ssl sha256 file
	echo '>'
	clear_screen
	echo '> echo "but eventually you will understand" | ../ft_ssl sha256 -p -r file'
	echo "but eventually you will understand" | ../ft_ssl sha256 -p -r file
	echo '>'
	clear_screen
	echo "> echo \"GL HF let's go\" | ../ft_ssl sha256 -p -s \"foo\" file"
	echo "GL HF let's go" | ../ft_ssl sha256 -p -s "foo" file
	echo '>'
	clear_screen
	echo '> echo "one more thing" | ../ft_ssl sha256 -r -p -s "foo" file -s "bar"'
	echo "one more thing" | ../ft_ssl sha256 -r -p -s "foo" file -s "bar"
	echo '>'
	clear_screen
	echo '> echo "just to be extra clear" | ../ft_ssl sha256 -r -q -p -s "foo" file'
	echo "just to be extra clear" | ../ft_ssl sha256 -r -q -p -s "foo" file
	echo '>'
	clear_screen
	echo '> echo "https://www.youtube.com/watch?v=w-5yAtMtrSM" > big_smoke_order_remix'
	echo "https://www.youtube.com/watch?v=w-5yAtMtrSM" > big_smoke_order_remix
	echo '> ./ft_ssl sha256 -q big_smoke_order_remix'
	../ft_ssl sha256 -q big_smoke_order_remix
	echo '>'
	clear_screen
	echo '> ./ft_ssl sha256 -s "wubba lubba dub dub"'
	../ft_ssl sha256 -s "wubba lubba dub dub"
	echo '>'
	clear_screen
}

for var in "$@"
do
	if [ "$var" = "md5" ]; then
		md5_suite
	fi
	if [ "$var" = "sha256" ]; then
		sha256_suite
	fi
done

rm file 2>/dev/null
rm big_smoke_order_remix 2>/dev/null