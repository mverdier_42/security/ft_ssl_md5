#!/bin/zsh
rm test 2>/dev/null

max=300

export LC_CTYPE=C

if [ $# -gt 0 ] && [ "$1" != "-v" ]
then
	max=$1
fi

echo "Testing messages with length from 0 to $(($max + 0))"

echo "Test for stdin"
for i in {0..$max}
do
	char=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 1 | head -n 1`
	echo -n $char >> test && cat test | ../ft_ssl sha384 > res_sha384 && cat test | openssl sha384 | cut -d ' ' -f2 > res_openssl
	DIFF=$(diff res_sha384 res_openssl)
	if [ "$2" = "-v" ] || [ "$1" = "-v" ]
	then
		cat res_sha384 res_openssl
		echo ""
	fi
	if [ "$DIFF" != "" ]
	then
		echo "diff at $i"
		cat res_sha384 res_openssl
		echo ""
	fi
done

echo ""
echo "Test for file"
rm test 2>/dev/null

for i in {0..$max}
do
	char=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 1 | head -n 1`
	echo -n $char >> test && ../ft_ssl sha384 test | cut -d ' ' -f4 > res_sha384 && openssl sha384 test | cut -d ' ' -f2 > res_openssl
	DIFF=$(diff res_sha384 res_openssl)
	if [ "$2" = "-v" ] || [ "$1" = "-v" ]
	then
		cat res_sha384 res_openssl
		echo ""
	fi
	if [ "$DIFF" != "" ]
	then
		echo "diff at $i"
		cat res_sha384 res_openssl
		echo ""
	fi
done

echo ""
echo "Test for string"
rm test 2>/dev/null

for i in {0..$max}
do
	char=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 1 | head -n 1`
	echo -n $char >> test && ../ft_ssl sha384 -s "`cat test`" | cut -d ' ' -f4 > res_sha384 && openssl sha384 test | cut -d ' ' -f2 > res_openssl
	DIFF=$(diff res_sha384 res_openssl)
	if [ "$2" = "-v" ] || [ "$1" = "-v" ]
	then
		cat res_sha384 res_openssl
		echo ""
	fi
	if [ "$DIFF" != "" ]
	then
		echo "diff at $i"
		cat res_sha384 res_openssl
		echo ""
	fi
done

rm test res_sha384 res_openssl 2>/dev/null
