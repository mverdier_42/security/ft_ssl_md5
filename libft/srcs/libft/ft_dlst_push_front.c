/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlst_push_front.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 14:37:15 by mverdier          #+#    #+#             */
/*   Updated: 2017/10/11 18:12:04 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

void	ft_dlst_push_front(t_dlist_b **dlist_b, t_dlist *new)
{
	if (!(*dlist_b)->first)
	{
		new->prev = new;
		new->next = new;
	}
	else
	{
		new->prev = (*dlist_b)->last;
		new->next = (*dlist_b)->first;
	}
	if (!(*dlist_b)->last)
		(*dlist_b)->last = new;
	else if ((*dlist_b)->last == (*dlist_b)->first)
	{
		(*dlist_b)->last->prev = new;
		(*dlist_b)->last->next = new;
	}
	else
		(*dlist_b)->last->next = new;
	if ((*dlist_b)->first)
		(*dlist_b)->first->prev = new;
	(*dlist_b)->first = new;
	(*dlist_b)->size++;
}
