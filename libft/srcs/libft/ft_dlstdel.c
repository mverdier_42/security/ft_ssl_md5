/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlstdel.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 15:39:40 by mverdier          #+#    #+#             */
/*   Updated: 2017/10/18 14:14:57 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <string.h>

void	ft_dlstdel(t_dlist_b **dlist_b, void (*del)(void *))
{
	t_dlist	*tmp;
	t_dlist	*temp;

	tmp = (*dlist_b)->first;
	while (tmp && tmp != (*dlist_b)->last)
	{
		temp = tmp->next;
		(*del)(tmp->content);
		free(tmp);
		tmp = temp;
	}
	if (tmp && tmp == (*dlist_b)->last)
	{
		(*del)(tmp->content);
		free(tmp);
	}
	(*dlist_b)->first = NULL;
	(*dlist_b)->last = NULL;
	(*dlist_b)->size = 0;
}
