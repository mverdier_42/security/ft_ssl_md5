/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isnumber.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/06 13:54:53 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/06 13:55:40 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_isnumber(int c)
{
	if (c >= '0' && c <= '9')
		return (1);
	return (0);
}
