/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:52:10 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/07 16:43:44 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

static char	*ft_stralloc(size_t i, size_t s_len, size_t j, char const *s)
{
	char *newstr;

	if (i >= s_len)
	{
		if ((newstr = ft_strnew(0)) == NULL)
			return (NULL);
	}
	else
	{
		if ((newstr = ft_strnew(s_len - i + 1)) == NULL)
			return (NULL);
	}
	if (i > s_len)
		newstr[0] = '\0';
	while (i < s_len)
	{
		newstr[j] = s[i];
		i++;
		j++;
	}
	if (i == s_len)
		newstr[j] = s[i];
	return (newstr);
}

char		*ft_strtrim(char const *s)
{
	size_t	i;
	size_t	j;
	size_t	s_len;

	if (!s)
		return (NULL);
	s_len = ft_strlen(s);
	i = 0;
	j = 0;
	while ((s[i] == ' ' || s[i] == ',' || s[i] == '\n' || s[i] == '\t') && s[i])
		i++;
	while ((s[s_len] == ' ' || s[s_len] == ',' || s[s_len] == '\n'
				|| s[s_len] == '\t' || s[s_len] == '\0') && s_len > 0)
		s_len--;
	return (ft_stralloc(i, s_len, j, s));
}
