/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nrealloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/17 19:20:21 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/07 17:34:16 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_nrealloc(char *buf, int newsize, int oldsize)
{
	char	*newbuf;

	newbuf = NULL;
	if ((newbuf = (char *)malloc(sizeof(char) * newsize)) == NULL)
		return (NULL);
	ft_bzero(newbuf, newsize);
	if (buf == NULL)
		return (newbuf);
	ft_memcpy(newbuf, buf, oldsize);
	free(buf);
	return (newbuf);
}
