/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:42:14 by mverdier          #+#    #+#             */
/*   Updated: 2017/01/30 19:25:51 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	ft_atoi(const char *nptr)
{
	int				i;
	int				neg;
	long int		result;

	neg = 1;
	i = 0;
	result = 0;
	while (nptr[i] && (nptr[i] == ' ' || nptr[i] == '\t' || nptr[i] == '\n'
				|| nptr[i] == '\f' || nptr[i] == '\v' || nptr[i] == '\r'))
		i++;
	if (nptr[i] && (nptr[i] < '0' || nptr[i] > '9')
			&& (nptr[i] != '+' && nptr[i] != '-'))
		return (0);
	if (nptr[i] == '+' || nptr[i] == '-')
	{
		if (nptr[i] == '-')
			neg = -1;
		i++;
	}
	while (nptr[i] >= '0' && nptr[i] <= '9')
		result = result * 10 + (nptr[i++] - 48);
	return (result * neg);
}
