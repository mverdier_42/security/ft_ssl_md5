/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/07 12:43:19 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/07 12:45:41 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

char	*ft_strnchr(const char *s, int c, int n)
{
	while (*s != c && *s != '\0' && n > 0)
	{
		s++;
		n--;
	}
	if (*s != c)
		return (NULL);
	return ((char *)s);
}
