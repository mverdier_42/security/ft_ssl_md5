/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_string.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/16 18:51:30 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/07 12:42:38 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STRING_H
# define FT_STRING_H

# define VAR_I 0
# define VAR_J 1
# define VAR_K 2
# define VAR_TMP 3

# include <string.h>
# include <stdbool.h>
# include <stdlib.h>

# include "ft_memory.h"

size_t			ft_strlen(const char *s);
char			*ft_strdup(const char *s);
char			*ft_strcpy(char *dest, const char *src);
char			*ft_strncpy(char *dest, const char *src, size_t n);
char			*ft_strcat(char *dest, const char *src);
char			*ft_strncat(char *dest, const char *src, size_t n);
size_t			ft_strlcat(char *s1, const char *s2, size_t size);
char			*ft_strchr(const char *s, int c);
char			*ft_strrchr(const char *s, int c);
char			*ft_strnchr(const char *s, int c, int n);
char			*ft_strstr(const char *big, const char *little);
char			*ft_strnstr(const char *big, const char *little, size_t len);
int				ft_strcmp(const char *s1, const char *s2);
int				ft_strncmp(const char *s1, const char *s2, size_t n);
char			*ft_strndup(const char *s, int len);
char			*ft_strdup_to_c(const char *s, int c);
int				ft_atoi(const char *nptr);
int				ft_toupper(int c);
int				ft_tolower(int c);

int				ft_isalpha(int c);
int				ft_isdigit(int c);
int				ft_isalnum(int c);
int				ft_isascii(int c);
int				ft_isprint(int c);
int				ft_islower(int c);
int				ft_isupper(int c);
int				ft_isnumber(int c);
int				ft_isblank(int c);
int				ft_isspace(int c);
int				ft_str_isdigit(const char *s);
int				ft_str_test_chars(char *str, int f(int));

char			*ft_strnew(size_t size);
void			ft_strdel(char **as);
void			ft_strclr(char *s);
void			ft_striter(char *s, void (*f)(char *));
void			ft_striteri(char *s, void (*f)(unsigned int, char *));
char			*ft_strmap(char const *s, char (*f)(char));
char			*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int				ft_strequ(char const *s1, char const *s2);
int				ft_strnequ(char const *s1, char const *s2, size_t n);
char			*ft_strsub(char const *s, unsigned int start, size_t len);
char			*ft_strjoin(char const *s1, char const *s2);
char			*ft_strtrim(char const *s);
char			*ft_strtrimc(char const *s, int c);
char			**ft_strsplit(char const *s, char c);
char			**ft_strsplit_str(char const *s, char *chars);
char			*ft_strextract(char *str, char c);
char			*ft_strrextract(char *str, char c);
void			ft_free_tab_str(char **tab);

void			ft_putchar(char c);
void			ft_putstr(char const *s);
void			ft_putendl(char const *s);
void			ft_putchar_fd(char c, int fd);
void			ft_putstr_fd(char const *s, int fd);
void			ft_putendl_fd(char const *s, int fd);

void			ft_strrev(char **str);
void			ft_strswap(char **s1, char **s2);
char			**ft_sortstrtab(char **tab);

#endif
