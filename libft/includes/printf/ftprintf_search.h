/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_search.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 15:52:39 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 16:03:54 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FTPRINTF_SEARCH_H
# define FTPRINTF_SEARCH_H

# include "ftprintf_params.h"

t_params	*search_param(const char *format);
void		search_option(const char *format, int *i, t_params *param);

#endif
