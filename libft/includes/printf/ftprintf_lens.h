/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_lens.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 15:45:41 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/05 19:54:54 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FTPRINTF_LENS_H
# define FTPRINTF_LENS_H

# include <inttypes.h>
# include <stddef.h>

int		bit_len(wchar_t c);
int		ft_intmax_len(intmax_t nb);
int		ft_uintmax_base_len(uintmax_t nb, unsigned int base);

#endif
