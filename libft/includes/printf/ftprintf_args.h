/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_args.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 15:44:17 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/05 19:51:44 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FTPRINTF_ARGS_H
# define FTPRINTF_ARGS_H

# include <string.h>
# include <stdlib.h>
# include <wctype.h>
# include <stdint.h>

enum			e_type
{
	INT,
	LINT,
	LLINT,
	SSIZE_T,
	UINT,
	ULINT,
	ULLINT,
	SIZE_T,
	CHAR,
	WINT_T,
	WCHAR_T,
	STR,
	INTMAX_T,
	UINTMAX_T,
	SINT,
	USINT,
	SCHAR,
	UCHAR,
	NUL
};

union			u_types
{
	intmax_t				i;
	uintmax_t				ui;
	char					c;
	char					*s;
	wint_t					wi_t;
	wchar_t					*wc_t;
};

typedef struct	s_args
{
	union u_types	arg;
	enum e_type		type;
	enum e_type		stype;
	int				len;
	struct s_args	*next;
}				t_args;

t_args			*push_back_new_arg(t_args **args);
void			delete_args(t_args **args);

int				arg_is_positive(t_args *arg);
int				arg_is_zero(t_args *arg);
int				arg_is_int(t_args *arg);
int				arg_is_uint(t_args *arg);
int				arg_is_char(t_args *arg);

#endif
