/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 17:29:12 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/14 19:03:15 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MD5_H
# define MD5_H

# include "libft.h"
# include "md_infos.h"
# include "operations.h"

# define DEBUG_MD5 0

typedef struct		s_md5
{
	const uint32_t	*r;
	const uint32_t	*k;
	uint32_t		h[4];
	char			*msg;
	uint32_t		size;
	uint32_t		end;
	uint32_t		bit_pad;
}					t_md5;

typedef struct		s_md5_val
{
	uint32_t		a;
	uint32_t		b;
	uint32_t		c;
	uint32_t		d;
	uint32_t		f;
	uint32_t		g;
	uint32_t		tmp;
}					t_md5_val;

void				parse_md5(char **av);
void				md5_usage(char *token);
const t_md_flags	*md5_get_flags(int *size);

void				md5_string(char ***av, char **token, int flags);
void				md5_stdin(char ***av, char **token, int flags);
void				md5_file(char *name, int flags);

void				md5_init(t_md5 *infos);
void				md5(t_md5 *infos, uint32_t len);
void				md5_hash(t_md5 *infos);

void				print_md5_blocks(char *str);
void				print_md5_vals(t_md5_val val, int i);
void				print_md5_processed_words(uint32_t *h);

#endif
