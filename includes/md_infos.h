/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md_infos.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 12:01:51 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/14 15:58:50 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MD_INFOS_H
# define MD_INFOS_H

# include "libft.h"

# define FLAG_P		0x00001
# define FLAG_Q		0x00010
# define FLAG_R		0x00100
# define FLAG_S		0x01000
# define FLAG_FILE	0x10000

typedef void	(*t_md_ptr)(char ***, char**, int);

typedef struct	s_md_flags
{
	char		flag;
	int			value;
	t_md_ptr	func;
}				t_md_flags;

void			parse_md_flags(char **av, const t_md_flags *(*get_flags)(int *),
				void (*usage)(char*), void (*md_file)(char *, int));
int				read_fd(int fd, char **buff, uint32_t *bufflen, uint32_t *size);

#endif
