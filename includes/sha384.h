/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha384.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 17:35:14 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 17:38:18 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHA384_H
# define SHA384_H

# include "sha512.h"

void				parse_sha384(char **av);
void				sha384_usage(char *token);
const t_md_flags	*sha384_get_flags(int *size);

void				sha384_string(char ***av, char **token, int flags);
void				sha384_stdin(char ***av, char **token, int flags);
void				sha384_file(char *name, int flags);

void				sha384_init(t_sha512 *infos);

#endif
