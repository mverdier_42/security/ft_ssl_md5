/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 17:12:07 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 18:37:34 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_H
# define FT_SSL_H

# include "md5.h"
# include "sha224.h"
# include "sha256.h"
# include "sha384.h"
# include "sha512.h"
# include "libft.h"

typedef void	(*t_cmd_ptr)(char **);

typedef enum	e_cmdtype
{
	STD,
	MD,
	CIPHER
}				t_cmdtype;

typedef struct	s_cmd
{
	char		*name;
	t_cmdtype	type;
	t_cmd_ptr	func;
}				t_cmd;

void			usage(void);
const t_cmd		*get_cmd(int *size);
void			parse_cmd(char **av);
void			print_cmd(const t_cmd *cmd, int size);

#endif
