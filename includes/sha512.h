/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 14:32:05 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 17:19:52 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHA512_H
# define SHA512_H

# include "libft.h"
# include "md_infos.h"
# include "operations.h"

# define DEBUG_SHA512 0

typedef struct		s_sha512
{
	uint64_t		k[80];
	uint64_t		h[8];
	uint64_t		w[80];
	char			*msg;
	uint32_t		size;
	uint32_t		end;
	uint32_t		bit_pad;
}					t_sha512;

typedef struct		s_sha512_val
{
	uint64_t		a;
	uint64_t		b;
	uint64_t		c;
	uint64_t		d;
	uint64_t		e;
	uint64_t		f;
	uint64_t		g;
	uint64_t		h;
	uint64_t		t1;
	uint64_t		t2;
}					t_sha512_val;

void				parse_sha512(char **av);
void				sha512_usage(char *token);
const t_md_flags	*sha512_get_flags(int *size);

void				sha512_string(char ***av, char **token, int flags);
void				sha512_stdin(char ***av, char **token, int flags);
void				sha512_file(char *name, int flags);

void				sha512_init(t_sha512 *infos);
void				sha512_fill_w(t_sha512 *infos);
void				sha512(t_sha512 *infos, uint32_t size);
void				sha512_hash(t_sha512 *infos);

uint64_t			sha512_ch(uint64_t e, uint64_t f, uint64_t g);
uint64_t			sha512_maj(uint64_t a, uint64_t b, uint64_t c);
uint64_t			sha512_sigma0(uint64_t n);
uint64_t			sha512_sigma1(uint64_t n);

void				print_sha512_blocks(char *str);
void				print_sha512_w(uint64_t *w);
void				print_sha512_val(t_sha512_val val, uint32_t i,
					uint32_t init);
void				print_sha512_block_processed(t_sha512_val val,
					t_sha512 infos);

#endif
