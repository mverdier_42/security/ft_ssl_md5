/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operations.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/09 16:28:36 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 17:09:33 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPERATIONS_H
# define OPERATIONS_H

# include <inttypes.h>

uint32_t	swap_uint32(uint32_t val);
uint64_t	swap_uint64(uint64_t val);
void		swap_uint128(void *val);
uint32_t	rot_left(uint32_t n, uint32_t i);
uint32_t	rot_right(uint32_t n, uint32_t i);
uint64_t	rot_left64(uint64_t n, uint64_t i);
uint64_t	rot_right64(uint64_t n, uint64_t i);

#endif
