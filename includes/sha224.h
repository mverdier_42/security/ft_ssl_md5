/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha224.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/15 18:34:00 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 18:35:16 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHA224_H
# define SHA224_H

# include "sha256.h"

void				parse_sha224(char **av);
void				sha224_usage(char *token);
const t_md_flags	*sha224_get_flags(int *size);

void				sha224_string(char ***av, char **token, int flags);
void				sha224_stdin(char ***av, char **token, int flags);
void				sha224_file(char *name, int flags);

void				sha224_init(t_sha256 *infos);

#endif
