/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 18:23:58 by mverdier          #+#    #+#             */
/*   Updated: 2019/10/15 15:43:39 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHA256_H
# define SHA256_H

# include "libft.h"
# include "md_infos.h"
# include "operations.h"

# define DEBUG_SHA256 0

typedef struct		s_sha256
{
	const uint32_t	*k;
	uint32_t		h[8];
	uint32_t		w[64];
	char			*msg;
	uint32_t		size;
	uint32_t		end;
	uint32_t		bit_pad;
}					t_sha256;

typedef struct		s_sha256_val
{
	uint32_t		a;
	uint32_t		b;
	uint32_t		c;
	uint32_t		d;
	uint32_t		e;
	uint32_t		f;
	uint32_t		g;
	uint32_t		h;
	uint32_t		t1;
	uint32_t		t2;
}					t_sha256_val;

void				parse_sha256(char **av);
void				sha256_usage(char *token);
const t_md_flags	*sha256_get_flags(int *size);

void				sha256_string(char ***av, char **token, int flags);
void				sha256_stdin(char ***av, char **token, int flags);
void				sha256_file(char *name, int flags);

void				sha256_init(t_sha256 *infos);
void				sha256_fill_w(t_sha256 *infos);
void				sha256(t_sha256 *infos, uint32_t size);
void				sha256_hash(t_sha256 *infos);

uint32_t			sha256_ch(uint32_t e, uint32_t f, uint32_t g);
uint32_t			sha256_maj(uint32_t a, uint32_t b, uint32_t c);
uint32_t			sha256_sigma0(uint32_t n);
uint32_t			sha256_sigma1(uint32_t n);

void				print_sha256_blocks(char *str);
void				print_sha256_w(uint32_t *w);
void				print_sha256_val(t_sha256_val val, uint32_t i,
					uint32_t init);
void				print_sha256_block_processed(t_sha256_val val,
					t_sha256 infos);

#endif
