# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/01/14 14:55:01 by mverdier          #+#    #+#              #
#    Updated: 2019/10/15 18:47:32 by mverdier         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# Colors.

ORANGE =	\033[1;33m   #It is actually Yellow, but i changed yellow to orange.

GREEN =		\033[1;32m

RED =		\033[1;31m

RES =		\033[0m

#------------------------------------------------------------------------------#
# List all sources, objects and includes files for ft_ssl_md5 project.

NAME = 		ft_ssl

SRCDIR =	./srcs

OBJDIR =	./objs

INCDIR =	./includes

LIBDIR =	./libft

LIBNAME =	libft.a

LIB =		$(LIBDIR)/$(LIBNAME)

LIBINC =	$(LIBDIR)/includes

SRCS =		$(SSL_SRCS) $(DIG_SRCS) $(MD5_SRCS) $(SHA256_SRCS)	\
			$(SHA512_SRCS) $(SHA384_SRCS) $(SHA224_SRCS)

OBJS =		$(SSL_OBJS) $(DIG_OBJS) $(MD5_OBJS) $(SHA256_OBJS)	\
			$(SHA512_OBJS) $(SHA384_OBJS) $(SHA224_OBJS)

INCS =		$(SSL_INCS) $(DIG_INCS) $(MD5_INCS) $(SHA256_INCS)	\
			$(SHA512_INCS) $(SHA384_INCS) $(SHA224_INCS)

#------------------------------------------------------------------------------#
# List all sources, objects and includes files for ft_ssl.

SSL_SRC =		main.c usage.c get_cmd.c parse_cmd.c print_cmd.c read_fd.c		\
				swap.c rotation.c

SSL_INC =		ft_ssl.h operations.h

SSL_SRCS =		$(SSL_SRC:%=$(SRCDIR)/%)

SSL_OBJS =		$(SSL_SRC:%.c=$(OBJDIR)/%.o)

SSL_INCS =		$(SSL_INC:%=$(INCDIR)/%)

#------------------------------------------------------------------------------#
# List all sources, objects and includes files for message digests.

DIG_SRC_DIR =	$(SRCDIR)/digest

DIG_OBJ_DIR =	$(OBJDIR)/digest

DIG_SRC =		parse_md_flags.c parse_md5.c parse_sha256.c			\
				parse_sha512.c parse_sha384.c parse_sha224.c

DIG_INC =		md_infos.h

DIG_SRCS =		$(DIG_SRC:%=$(DIG_SRC_DIR)/%)

DIG_OBJS =		$(DIG_SRC:%.c=$(DIG_OBJ_DIR)/%.o)

DIG_INCS =		$(DIG_INC:%=$(INCDIR)/%)

#------------------------------------------------------------------------------#
# List all sources, objects and includes files for md5.

MD5_SRC_DIR =	$(DIG_SRC_DIR)/md5

MD5_OBJ_DIR =	$(DIG_OBJ_DIR)/md5

MD5_SRC =		md5_get_flags.c md5_debug.c md5_file.c md5_hash.c	\
				md5_stdin.c md5_string.c md5_usage.c md5.c md5_init.c

MD5_INC =		md5.h

MD5_SRCS =		$(MD5_SRC:%=$(MD5_SRC_DIR)/%)

MD5_OBJS =		$(MD5_SRC:%.c=$(MD5_OBJ_DIR)/%.o)

MD5_INCS =		$(MD5_INC:%=$(INCDIR)/%)

#------------------------------------------------------------------------------#
# List all sources, objects and includes files for sha256.

SHA256_SRC_DIR =	$(DIG_SRC_DIR)/sha256

SHA256_OBJ_DIR =	$(DIG_OBJ_DIR)/sha256

SHA256_SRC =		sha256_get_flags.c sha256_debug.c sha256_file.c 		\
					sha256_init.c sha256_hash.c sha256_stdin.c				\
					sha256_string.c sha256_usage.c sha256.c sha256_fill_w.c	\
					sha256_hash_functions.c

SHA256_INC =		sha256.h

SHA256_SRCS =		$(SHA256_SRC:%=$(SHA256_SRC_DIR)/%)

SHA256_OBJS =		$(SHA256_SRC:%.c=$(SHA256_OBJ_DIR)/%.o)

SHA256_INCS =		$(SHA256_INC:%=$(INCDIR)/%)

#------------------------------------------------------------------------------#
# List all sources, objects and includes files for sha512.

SHA512_SRC_DIR =	$(DIG_SRC_DIR)/sha512

SHA512_OBJ_DIR =	$(DIG_OBJ_DIR)/sha512

SHA512_SRC =		sha512_get_flags.c sha512_debug.c sha512_file.c 		\
					sha512_init.c sha512_hash.c sha512_stdin.c				\
					sha512_string.c sha512_usage.c sha512.c sha512_fill_w.c	\
					sha512_hash_functions.c

SHA512_INC =		sha512.h

SHA512_SRCS =		$(SHA512_SRC:%=$(SHA512_SRC_DIR)/%)

SHA512_OBJS =		$(SHA512_SRC:%.c=$(SHA512_OBJ_DIR)/%.o)

SHA512_INCS =		$(SHA512_INC:%=$(INCDIR)/%)

#------------------------------------------------------------------------------#
# List all sources, objects and includes files for sha384.

SHA384_SRC_DIR =	$(DIG_SRC_DIR)/sha384

SHA384_OBJ_DIR =	$(DIG_OBJ_DIR)/sha384

SHA384_SRC =		sha384_get_flags.c sha384_file.c sha384_init.c	\
					sha384_stdin.c sha384_string.c sha384_usage.c

SHA384_INC =		sha384.h

SHA384_SRCS =		$(SHA384_SRC:%=$(SHA384_SRC_DIR)/%)

SHA384_OBJS =		$(SHA384_SRC:%.c=$(SHA384_OBJ_DIR)/%.o)

SHA384_INCS =		$(SHA384_INC:%=$(INCDIR)/%)

#------------------------------------------------------------------------------#
# List all sources, objects and includes files for sha224.

SHA224_SRC_DIR =	$(DIG_SRC_DIR)/sha224

SHA224_OBJ_DIR =	$(DIG_OBJ_DIR)/sha224

SHA224_SRC =		sha224_get_flags.c sha224_file.c sha224_init.c	\
					sha224_stdin.c sha224_string.c sha224_usage.c

SHA224_INC =		sha224.h

SHA224_SRCS =		$(SHA224_SRC:%=$(SHA224_SRC_DIR)/%)

SHA224_OBJS =		$(SHA224_SRC:%.c=$(SHA224_OBJ_DIR)/%.o)

SHA224_INCS =		$(SHA224_INC:%=$(INCDIR)/%)

#------------------------------------------------------------------------------#
# List all compilation variables.

CC =		gcc

CFLAGS =	-Wall			\
			-Wextra			\
			-Werror			\
			-g

INCFLAGS =	-I $(INCDIR)	\
			-I $(LIBINC)

LFLAGS =	-L $(LIBDIR) -l$(LIBNAME:lib%.a=%)

FLAGS =		$(CFLAGS)		\
			$(INCFLAGS)

#------------------------------------------------------------------------------#
# List all rules used to make libft.a

all:
	@$(MAKE) $(NAME)

$(NAME): $(OBJS) $(LIB)
	@$(MAKE) printname
	@printf "%-15s%s\n" Linking $@
	@$(CC) $(CFLAGS) $^ -o $@ $(LFLAGS)
	@printf "$(GREEN)"
	@echo "Compilation done !"
	@printf "$(RES)"

$(LIB):
	@$(MAKE) -C $(LIBDIR)

# $(OBJS): $(INCS)
$(SSL_OBJS): $(SSL_INCS)

$(DIG_OBJS): $(DIG_INCS)

$(MD5_OBJS): $(MD5_INCS)

$(SHA256_OBJS): $(SHA256_INCS)

$(SHA512_OBJS): $(SHA512_INCS)

$(SHA384_OBJS): $(SHA384_INCS)

$(SHA224_OBJS): $(SHA224_INCS)

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	@mkdir -p $(OBJDIR) $(DIG_OBJ_DIR) $(MD5_OBJ_DIR) $(SHA256_OBJ_DIR)	\
		$(SHA512_OBJ_DIR) $(SHA384_OBJ_DIR) $(SHA224_OBJ_DIR)
	@$(MAKE) printname
	@printf "%-15s%s\n" Compiling $@
	@$(CC) $(FLAGS) -o $@ -c $<

$(DIG_OBJ_DIR)/%.o: $(DIG_SRC_DIR)/%.c
	mkdir -p $(DIG_OBJ_DIR)
	@$(MAKE) printname
	@printf "%-15s%s\n" Compiling $@
	@$(CC) $(FLAGS) -o $@ -c $<

$(MD5_OBJ_DIR)/%.o: $(MD5_SRC_DIR)/%.c
	@mkdir -p $(MD5_OBJ_DIR)
	@$(MAKE) printname
	@printf "%-15s%s\n" Compiling $@
	@$(CC) $(FLAGS) -o $@ -c $<

$(SHA256_OBJ_DIR)/%.o: $(SHA256_SRC_DIR)/%.c
	@mkdir -p $(SHA256_OBJ_DIR)
	@$(MAKE) printname
	@printf "%-15s%s\n" Compiling $@
	@$(CC) $(FLAGS) -o $@ -c $<

$(SHA512_OBJ_DIR)/%.o: $(SHA512_SRC_DIR)/%.c
	@mkdir -p $(SHA512_OBJ_DIR)
	@$(MAKE) printname
	@printf "%-15s%s\n" Compiling $@
	@$(CC) $(FLAGS) -o $@ -c $<

$(SHA384_OBJ_DIR)/%.o: $(SHA384_SRC_DIR)/%.c
	@mkdir -p $(SHA384_OBJ_DIR)
	@$(MAKE) printname
	@printf "%-15s%s\n" Compiling $@
	@$(CC) $(FLAGS) -o $@ -c $<

$(SHA224_OBJ_DIR)/%.o: $(SHA224_SRC_DIR)/%.c
	@mkdir -p $(SHA224_OBJ_DIR)
	@$(MAKE) printname
	@printf "%-15s%s\n" Compiling $@
	@$(CC) $(FLAGS) -o $@ -c $<

printname:
	@printf "$(ORANGE)"
	@printf "[%-15s " "$(NAME)]"
	@printf "$(RES)"

clean:
	@$(MAKE) -C $(LIBDIR) clean
	@$(MAKE) printname
	@echo Suppressing obj files
	@printf "$(RED)"
	rm -rf $(OBJS)
	@rm -rf $(OBJDIR)
	@printf "$(RES)"

fclean: clean
	@$(MAKE) -C $(LIBDIR) fclean
	@$(MAKE) printname
	@echo Suppressing $(NAME)
	@printf "$(RED)"
	rm -rf $(NAME)
	@printf "$(RES)"

re: fclean
	@$(MAKE) all

#------------------------------------------------------------------------------#
# List of all my optionnals but usefull rules.

NORM = `norminette $(SRCS) $(INCS) | grep -B1 Error | cat`

norm:
	@$(MAKE) printname
	@echo "Passage de la norminette :"
	@if [ "$(NORM)" == "`echo ""`" ];									\
		then															\
			echo "$(GREEN)Tous les fichiers sont a la norme !$(RES)";	\
		else															\
			echo "$(RED)$(NORM)$(RES)";									\
	fi

# A rule to make git add easier

git:
	@$(MAKE) -C $(LIBDIR) git
	@$(MAKE) printname
	@echo Adding files to git repository
	@printf "$(GREEN)"
	git add $(SRCS) $(INCS) Makefile
	@printf "$(RES)"
	@#git status

.PHONY: all clean re fclean git norm check
